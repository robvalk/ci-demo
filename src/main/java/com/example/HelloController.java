package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rvalk on 13/03/2017.
 */
@RestController
public class HelloController {

  @RequestMapping("/hello")
  public String index() {
    return "Hello world v1";
  }
}
